function allReady(){
	console.log("|--------------------------------------------|")
	console.log("|         Starting Character Demo            |")
	console.log("|--------------------------------------------|")
	if(scene){
		// Add asset paths
		scene.addAssetPath('mesh', 'ChrBrad')
		//scene.addAssetPath('motion', 'ChrBrad')
		scene.addAssetPath('motion', 'ChrBrad')
		scene.loadAssets()

		// Set scene parameters and camera
		console.log('Configuring scene parameters and camera')
		scene.setScale(1.0)
		//scene.setBoolAttribute("enableAlphaBlend", true)
		scene.setBoolAttribute('internalAudio', true)
		defaultViewer.defaultViewer();
		var camera = Module.getCamera()
		camera.setEye(0, 1.71, 1.86)
		camera.setCenter(0, 1, 0.01)
		camera.setUpVector(new Module.SrVec(0, 1, 0))
		camera.setScale(1)
		camera.setFov(1.0472)
		camera.setFarPlane(100)
		camera.setNearPlane(0.1)
		camera.setAspectRatio(0.966897)
		var cameraPos = new Module.SrVec(0, 1.6, 10)
		scene.getPawn('camera').setPosition(cameraPos)
		defaultInterface.defaultInterface();

		// Set up joint map for Brad
		console.log('Setting up joint map and configuring Brad\'s skeleton')
		zebra2map.zebra2Map();
		var zebra2Map = scene.getJointMapManager().getJointMap('zebra2')
		var bradSkeleton = scene.getSkeleton('ChrBrad.sk')
		zebra2Map.applySkeleton(bradSkeleton)
		zebra2Map.applyMotionRecurse('ChrMaarten')

		// Establish lip syncing data set
		console.log('Establishing lip syncing data set')
		initDiphoneDefault.initDiphoneDefault()

		// Set up face definition
		console.log('Setting up face definition')
		// Brad's face definition
		var bradFace = scene.createFaceDefinition('ChrBrad')
		bradFace.setFaceNeutral('ChrBrad@face_neutral')

		bradFace.setAU(1,  "left",  "ChrBrad@001_inner_brow_raiser_lf")
		bradFace.setAU(1,  "right", "ChrBrad@001_inner_brow_raiser_rt")
		bradFace.setAU(2,  "left",  "ChrBrad@002_outer_brow_raiser_lf")
		bradFace.setAU(2,  "right", "ChrBrad@002_outer_brow_raiser_rt")
		bradFace.setAU(4,  "left",  "ChrBrad@004_brow_lowerer_lf")
		bradFace.setAU(4,  "right", "ChrBrad@004_brow_lowerer_rt")
		bradFace.setAU(5,  "both",  "ChrBrad@005_upper_lid_raiser")
		bradFace.setAU(6,  "both",  "ChrBrad@006_cheek_raiser")
		bradFace.setAU(7,  "both",  "ChrBrad@007_lid_tightener")
		bradFace.setAU(10, "both",  "ChrBrad@010_upper_lip_raiser")
		bradFace.setAU(12, "left",  "ChrBrad@012_lip_corner_puller_lf")
		bradFace.setAU(12, "right", "ChrBrad@012_lip_corner_puller_rt")
		bradFace.setAU(25, "both",  "ChrBrad@025_lips_part")
		bradFace.setAU(26, "both",  "ChrBrad@026_jaw_drop")
		bradFace.setAU(45, "left",  "ChrBrad@045_blink_lf")
		bradFace.setAU(45, "right", "ChrBrad@045_blink_rt")

		bradFace.setViseme("open",    "ChrBrad@open")
		bradFace.setViseme("W",       "ChrBrad@W")
		bradFace.setViseme("ShCh",    "ChrBrad@ShCh")
		bradFace.setViseme("PBM",     "ChrBrad@PBM")
		bradFace.setViseme("FV",      "ChrBrad@FV")
		bradFace.setViseme("wide",    "ChrBrad@wide")
		bradFace.setViseme("tBack",   "ChrBrad@tBack")
		bradFace.setViseme("tRoof",   "ChrBrad@tRoof")
		bradFace.setViseme("tTeeth",  "ChrBrad@tTeeth")

		bradFace.setAU(1,  "left",  "")
		bradFace.setAU(1,  "right", "")
		bradFace.setAU(2,  "left",  "")
		bradFace.setAU(2,  "right", "")
		bradFace.setAU(4,  "left",  "")
		bradFace.setAU(4,  "right", "")
		bradFace.setAU(5,  "both",  "")
		bradFace.setAU(6,  "both",  "")
		bradFace.setAU(7,  "both",  "")
		bradFace.setAU(10, "both",  "")
		bradFace.setAU(12, "left",  "")
		bradFace.setAU(12, "right", "")
		bradFace.setAU(25, "both",  "")
		bradFace.setAU(26, "both",  "")
		bradFace.setAU(45, "left",  "")
		bradFace.setAU(45, "right", "")

		bradFace.setViseme("open",    "")
		bradFace.setViseme("W",       "")
		bradFace.setViseme("ShCh",    "")
		bradFace.setViseme("PBM",     "")
		bradFace.setViseme("FV",      "")
		bradFace.setViseme("wide",    "")
		bradFace.setViseme("tBack",   "")
		bradFace.setViseme("tRoof",   "")
		bradFace.setViseme("tTeeth",  "")
		
		bradFace.setViseme("1",  "ChrBrad@face_neutral") // eye blink l
		bradFace.setViseme("2",  "ChrBrad@face_neutral") // eye blink r
		bradFace.setViseme("9",  "ChrBrad@face_neutral") // eye open l
		bradFace.setViseme("10",  "ChrBrad@face_neutral") // eye open r
		bradFace.setViseme("15",  "ChrBrad@face_neutral") // brow down l
		bradFace.setViseme("16",  "ChrBrad@face_neutral") // brow down r
		bradFace.setViseme("17",  "ChrBrad@face_neutral") // inner brows up 
		bradFace.setViseme("18",  "ChrBrad@face_neutral") // upper brow l
		bradFace.setViseme("19",  "ChrBrad@face_neutral") // upper brow r
		bradFace.setViseme("20",  "ChrBrad@face_neutral") // jaw open
		bradFace.setViseme("25",  "ChrBrad@face_neutral") // lips upper l
		bradFace.setViseme("26",  "ChrBrad@face_neutral") // lips upper r
		bradFace.setViseme("31",  "ChrBrad@face_neutral") // smile l
		bradFace.setViseme("32",  "ChrBrad@face_neutral") // smile r


		console.log('Adding character into scene')
		// Set up Brad
		var brad = scene.createCharacter('ChrBrad', '')
		var bradSkeleton = scene.createSkeleton('ChrBrad.sk')
		brad.setSkeleton(bradSkeleton)
		// Set position
		var bradPos = new Module.SrVec(0, 0, 0)
		brad.setPosition(bradPos)
		// Set facing direction
		var bradFacing = new Module.SrVec(0, 0, 0)
		brad.setHPR(bradFacing)
		// Set face definition
		brad.setFaceDefinition(bradFace)
		// Set standard controller
		brad.createStandardControllers()
		// Deformable mesh
		brad.setDoubleAttribute('deformableMeshScale', .01)
		brad.setStringAttribute('deformableMesh', 'ChrBrad.dae')
		//brad.setStringAttribute('deformableMesh', 'ChrBrad.dae')

		// Lip syncing diphone setup
		brad.setStringAttribute('lipSyncSetName', 'default')
		brad.setBoolAttribute('usePhoneBigram', true)
		brad.setVoice('remote')
		brad.setVoiceCode('Microsoft|Anna')

		// setup locomotion
		BehaviorSetMaleMocapLocomotion.setupBehaviorSet()
		BehaviorSetMaleMocapLocomotion.retargetBehaviorSet('ChrBrad')

		// setup gestures
		BehaviorSetGestures.setupBehaviorSet()
		BehaviorSetGestures.retargetBehaviorSet('ChrBrad')
		
		// setup reach 
		BehaviorSetReaching.setupBehaviorSet()
		BehaviorSetReaching.retargetBehaviorSet('ChrBrad')
		
		// Turn on GPU deformable geometry
		brad.setStringAttribute("displayType", "mesh")

		// Set up steering
		console.log('Setting up steering')
		var steerManager = scene.getSteerManager()
		steerManager.setEnable(false)
		brad.setBoolAttribute('steering.pathFollowingMode', false) // disable path following mode so that obstacles will be respected
		steerManager.setEnable(true)
		var faceShiftManager = scene.getFaceShiftManager()
		faceShiftManager.setStringAttribute('targetCharacter','ChrBrad')
		// Start the simulation
		console.log('Starting the simulation')
		sim.start()

		bml.execBML('ChrBrad', '<body posture="ChrBrad@Idle01"/>')
		bml.execBML('ChrBrad', '<saccade mode="listen"/>')
		//bml.execBML('ChrBrad', '<gaze sbm:handle="brad" target="camera"/>')

		// create a controller
		var MyController = Module.JavascriptController.extend('JavascriptController', {
			init : function(){
				this.addChannel("au_1_left", "XPos")
				this.addChannel("au_1_right", "XPos")
				this.addChannel("au_2_left", "XPos")
				this.addChannel("au_2_right", "XPos")
				this.addChannel("au_4_left", "XPos")
				this.addChannel("au_4_right", "XPos")
				this.addChannel("au_5", "XPos")
				this.addChannel("au_10", "XPos")
				this.addChannel("au_45_left", "XPos")
				this.addChannel("au_45_right", "XPos")
				this.addChannel("open", "XPos")
				this.addChannel("W", "XPos")
				this.addChannel("ShCh", "XPos")
				this.addChannel("wide", "XPos")
				this.addChannel("PBM", "XPos")
				this.addChannel("FV", "XPos")
				this.addChannel("skullbase", "Quat")
				this.addChannel("1", "XPos")
				this.addChannel("2", "XPos")
				this.addChannel("9", "XPos")
				this.addChannel("10", "XPos")
				this.addChannel("15", "XPos")
				this.addChannel("16", "XPos")
				this.addChannel("17", "XPos")
				this.addChannel("18", "XPos")
				this.addChannel("19", "XPos")		
				this.addChannel("20", "XPos")	
			},
			
			evaluate : function(){
				// every time step, get the value from one channel and then modify another
				var faceShiftManager = scene.getFaceShiftManager()
				// blink
				var val = faceShiftManager.getCoeffValue("1")
				this.setChannelValue("au_45_left", val)
				val = faceShiftManager.getCoeffValue("2")
				this.setChannelValue("au_45_right", val)
				
				//console.log "BLINK " + str(val)
				
				// eye opener
				val = faceShiftManager.getCoeffValue("9")
				this.setChannelValue("au_5", val)
				
				// eyebrows down opener
				val = faceShiftManager.getCoeffValue("15")
				this.setChannelValue("au_4_left", val)
				val = faceShiftManager.getCoeffValue("16")
				this.setChannelValue("au_4_right", val)	
				
				// eyebrows up
				val = faceShiftManager.getCoeffValue("17")
				this.setChannelValue("au_1_left", val)
				this.setChannelValue("au_1_right", val)			
				
				// outer eyebrows up
				val = faceShiftManager.getCoeffValue("18")
				this.setChannelValue("au_2_left", val)
				val = faceShiftManager.getCoeffValue("19")
				this.setChannelValue("au_2_right", val)			
			
				// LipsPucker
				val = faceShiftManager.getCoeffValue("41")
				this.setChannelValue("W", val)
				//console.log 'W = ' + str(val)
				
				// LipsFunnel
				val = faceShiftManager.getCoeffValue("42")
				this.setChannelValue("ShCh", val)
				//console.log 'ShCh = ' + str(val)
				
					
				// jaw open
				val = faceShiftManager.getCoeffValue("20")
				this.setChannelValue("open", val)
				//console.log 'open = ' + str(val)		
				
				// LipsUpperClose
				val = faceShiftManager.getCoeffValue("29")
				this.setChannelValue("PBM", val)
				//console.log 'PBM = ' + str(val)
				
				// LipsUpperClose
				val = faceShiftManager.getCoeffValue("46")
				this.setChannelValue("FV", val)
				//console.log 'FV = ' + str(val)
				
				// MouthDimple
				val = faceShiftManager.getCoeffValue("33")
				this.setChannelValue("wide", val)
				//console.log 'wide = ' + str(val)
				//val = faceShiftManager.getCoeffValue("34")
				
				var rot = faceShiftManager.getHeadRotation()
				//console.log 'rot = ' + str(rot.getData(0)) + ', ' + str(rot.getData(1)) + ', ' + str(rot.getData(2)) + ', ' + str(rot.getData(3))
				this.setChannelQuatGlobal('skullbase',rot)
			}
			
		});
				
		// instantiate this controller once for each character
		var myc = new MyController
		// get the character
		var character = scene.getCharacter("ChrBrad")
		// run this controller in position 15 (I can explain more about this)
		character.addController(15, myc) 
		// you can enable/disable the controller like this:
		//myc.setIgnore(True)

		sim.resume()

	}else{
		console.error("Scene does not exist!");
	}
}
